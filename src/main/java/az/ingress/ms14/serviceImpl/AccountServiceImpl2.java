package az.ingress.ms14.serviceImpl;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.repository.PhoneNumberRepository;
import az.ingress.ms14.service.AccountService;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl2 implements AccountService {

    //CacheManager edit
    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final PhoneNumberRepository phoneNumberRepository;
    private final CacheManager cacheManager;

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account getById(Long id) {
        log.info("getbyid method start");
        Cache cache = cacheManager.getCache("account");
        Account accountFromCache = cache.get(id, Account.class);
        if (accountFromCache == null) {
            Account account = accountRepository.findById(id).get();
            cache.put(id, account);
            return account;
        }
        log.info("getbyid end");
        return accountFromCache;

    }

    public Account update(Account account) {
        Cache cache = cacheManager.getCache("account");
        Account accountFromCache = cache.get(account.getId(), Account.class);
        if (accountFromCache != null) {
            accountFromCache.setName(account.getName());
            accountFromCache.setBalance(account.getBalance());
            accountRepository.save(accountFromCache);
            return accountFromCache;
        } else {
            Account accountFromRepo = accountRepository.findById(account.getId()).orElseThrow();
            accountFromRepo.setName(account.getName());
            accountFromRepo.setBalance(account.getBalance());
            cache.put(account.getId(), accountFromRepo);
            accountRepository.save(accountFromRepo);
            return accountFromRepo;
        }
    }

    public Account delete(Long id) {
        Cache cache = cacheManager.getCache("account");
        Account deletedAccount = cache.get(id, Account.class);
        if (deletedAccount != null) {
            cache.evict(id);
            accountRepository.deleteById(id);
            return deletedAccount;
        } else {
            Optional<Account> optionalAccount = accountRepository.findById(id);
            if (optionalAccount.isPresent()) {
                Account account = optionalAccount.get();
                cache.put(id, account);
                accountRepository.deleteById(id);
                return account;
            }
        }
        return deletedAccount;
    }
}

