package az.ingress.ms14.serviceImpl;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.repository.PhoneNumberRepository;
import az.ingress.ms14.service.AccountService;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl3 implements AccountService {

    //RedisTemplate edit
    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final PhoneNumberRepository phoneNumberRepository;
    private final RedisTemplate<String, Object> redisTemplate;

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    @Transactional
    public Account getById(Long id) {

        var o = (Account) redisTemplate.opsForValue().get(String.valueOf(id));
        if (o != null) {
            return o;
        }
        Account account = accountRepository.findById(id).get();
        redisTemplate.opsForValue().set(String.valueOf(id), account, Duration.ofSeconds(60));
        return account;
    }

    @Transactional
    public Account update(Account account) {

        Object o = redisTemplate.opsForValue().get(String.valueOf(account.getId()));
        if (o != null) {
            redisTemplate.opsForValue().set(String.valueOf(account.getId()), account);
            return account;
        } else {
            throw new IllegalArgumentException("Account with id " + account.getId() + " does not exist");
        }
    }

    @Transactional
    public Account delete(Long id) {
        var o = (Account) redisTemplate.opsForValue().get(String.valueOf(id));
        if (o != null) {
            redisTemplate.delete(String.valueOf(id));
        }
        Account account = accountRepository.findById(id).orElse(null);
        if (account != null) {
            accountRepository.delete(account);
        } else {
            throw new IllegalArgumentException("Account with id " + o.getId() + " does not exist");
        }
        return o;
    }
}

