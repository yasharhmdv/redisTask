package az.ingress.ms14.serviceImpl;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.model.Employee;
import az.ingress.ms14.model.PhoneNumber;
import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.repository.PhoneNumberRepository;
import az.ingress.ms14.service.AccountService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.validation.constraints.Null;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final EntityManagerFactory entityManagerFactory;
    private final PhoneNumberRepository phoneNumberRepository;

    public static Map<Long, Account> cache = new HashMap<>();


    @Cacheable(cacheNames = "allAccount", key = "#root.methodName")
    public List<Account> findAll(){
        return accountRepository.findAll();
    }

    @Cacheable(key = "#id", cacheNames = "account")
    public Account getById(Long id){
        log.info("getnyid meyhod");
        Account account = accountRepository.findById(id).get();
        log.info("getbyid end");
        return account;
    }

//    @Cacheable(key = "#employee.name+#employee.surname+#employee.age", cacheNames = "account")
//    public Account getById(Long id, Employee employee){
//        return accountRepository.findById(id).get();
//    }


    @Transactional
    @CachePut(cacheNames = "account", key = "#account.id")
    public Account update(Account account) {
        Account account1 = accountRepository.findById(account.getId()).orElseThrow();
        account1.setName(account.getName());
        account1.setBalance(account.getBalance());
        return  account1;
    }

    @CacheEvict(key = "#id", cacheNames = "account")
    public Account delete(Long id) {
        Account account = accountRepository.findById(id).orElseThrow();
        accountRepository.delete(account);
        return account;
    }
}
