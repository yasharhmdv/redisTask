package az.ingress.ms14.service;

import az.ingress.ms14.model.PhoneNumber;
import az.ingress.ms14.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PhoneService {
    private final PhoneNumberRepository phoneNumberRepository;

    @Cacheable(key = "#id", cacheNames = "phoneNumber")
    public PhoneNumber getById(Long id) {
        return phoneNumberRepository.findById(id).get();
    }
}
