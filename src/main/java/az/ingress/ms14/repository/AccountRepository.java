package az.ingress.ms14.repository;

import az.ingress.ms14.model.Account;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountRepository extends JpaRepository<Account, Long> {

//    Optional<Account> findById(Long id);


//    @Override
//    //@Cacheable(cacheNames = "account", key = "#id")
//    Optional<Account> findById(Long id);
}
