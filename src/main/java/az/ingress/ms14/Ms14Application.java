package az.ingress.ms14;

import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.repository.PhoneNumberRepository;
import az.ingress.ms14.serviceImpl.AccountServiceImpl;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Ms14Application implements CommandLineRunner {

    private final AccountServiceImpl accountService;
    private final AccountRepository accountRepository;
    private final EntityManager entityManager;
    private final EntityManagerFactory emf;
    private final PhoneNumberRepository phoneNumberRepository;


    public static void main(String[] args) {
        SpringApplication.run(Ms14Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {



     /*     EntityManager entityManager = emf.createEntityManager();
         entityManager.getTransaction().begin();
          Account reference = entityManager.getReference(Account.class, 5L);
          reference.setName("Eli");
          accountService.method1();*/
//
//        Account referenceById = accountRepository.getReferenceById(5L);
//        log.info("setname");
//        referenceById.setName("VEli");
//        log.info("metod end");

//        SessionFactory sf = emf.unwrap(SessionFactory.class);
//        Session session = sf.openSession();
//
//        PhoneNumber load = session.getReference(PhoneNumber.class, 4L);
//        session.close();
//        System.out.println(load.getPhone());
//        EntityManager entityManager = emf.createEntityManager();

//          Account account = new Account();
//          account.setName("Ehmed");
//          account.setBalance(300.0);
//
//        PhoneNumber referenceById = phoneNumberRepository.getReferenceById(4L);
//
//        account.getPhoneNumberList().add(referenceById);
//
//        accountRepository.save(account);

//        Account a = new Account();
//        a.setName("Rovshan");
//        a.setBalance(400.0);
//
//        a = accountRepository.save(a);
//        PhoneNumber p = new PhoneNumber();
//        p.setAccount(a);
//        p.setPhone("56211799");
//        phoneNumberRepository.save(p);

//        Account a = accountRepository.findById(5L).get();
//        accountRepository.delete(a);
//
//        System.out.println(a.getPhoneNumberList());


//        log.info("begin to count");
//        accountService.assignPhoneToAccountWithReference(a.getId(), List.of(1L,2L,3L,4L));
//
//
//        SessionFactory sf = emf.unwrap(SessionFactory.class);
//        Session session = sf.openSession();
//
//        session.beginTransaction();
//        log.info("#1 getting account with id 1");
//        var account = session.get(Account.class, 1L);
//        log.info("account is {}", account);
//        log.info("waiting 10 sec");
//        Thread.sleep(10000);
//
//        log.info("#2 getting account with id 1");
//        session.evict(account);
//        var account1 = session.get(Account.class, 1L);
//        //select * from account; //id=1, id=5
//        log.info("account is {}", account1);
//        session.getTransaction().commit();
//        session.close();


        //Manual Cash
//        Account byCash = accountService.getByCash(1L);
//        Account byCash2 = accountService.getByCash(1L);


//        EntityManager em = emf.createEntityManager();
//        em.getTransaction().begin();
//        Account account = em.find(Account.class, 1L);
//        account.setName("Tabriz");
//        em.flush();
//        em.detach(account);
//        account.setName("Fuad");
//        em.merge(account);
//        em.getTransaction().commit();
//        em.close();
    }
}
