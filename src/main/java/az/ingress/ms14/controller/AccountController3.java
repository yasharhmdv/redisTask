package az.ingress.ms14.controller;

import az.ingress.ms14.model.Account;
import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.serviceImpl.AccountServiceImpl2;
import az.ingress.ms14.serviceImpl.AccountServiceImpl3;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController3 {

    private final AccountRepository accountRepository;
    private final AccountServiceImpl3 accountService;

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable Long id) {
        return accountService.getById(id);
    }

    @GetMapping
    public List<Account> findAll(){
        return accountService.findAll();
    }
    @PutMapping
    public Account update(@RequestBody Account account){
       return accountService.update(account);
    }
    @DeleteMapping("/{id}")
    public Account delete(@PathVariable Long id){
        return accountService.delete(id);
    }
}
