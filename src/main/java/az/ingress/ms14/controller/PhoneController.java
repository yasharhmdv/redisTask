package az.ingress.ms14.controller;

import az.ingress.ms14.model.PhoneNumber;
import az.ingress.ms14.repository.AccountRepository;
import az.ingress.ms14.service.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/phone")
@RequiredArgsConstructor
public class PhoneController {

    private final AccountRepository accountRepository;
    private final PhoneService phoneService;

    @GetMapping("/{id}")
    @Cacheable(key = "#id", cacheNames = "account")
    public PhoneNumber getAccount(@PathVariable Long id) {
         return phoneService.getById(id);
    }
}
